(ns cljc.core
  (:refer-clojure :exclude [slurp read-string]))

(defmacro slurp [file]
  (clojure.core/slurp file))
