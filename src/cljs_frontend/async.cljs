(ns cljs-frontend.async
  (:require [cljs.core.async :refer [chan put! close!]]))

(defn promise-ch [promise]
    (let [c (chan)]
      (-> (.then promise #(.json %))
          ;; put the result on the channel and close it
          (.then (fn [json] (put! c (js->clj json :keywordize-keys true) #(close! c)))))
      ;; ultimately return the channel
      c))
